# Snake
This is an example of using Raylib and BQN to make a simple game.
Snake made with Raylib and BQN. barebonesraylib.bqn is a thin wrapper on a portion of raylib.

## Controls
| Button        | Function          |
|---------------|-------------------|
| Right Shift   | Pause/restart     |
| Left Arrow	| turn left			|
| Right Arrow	| turn right		|
| Up arrow		| speed up      	|
| Down Arrow	| slow down 		|

# Running
1. Move a libraylib.so (or libraylib.dll if on windows) into this directory.
2. While in this directory: BQN snake.bqn

Don't worry if you haven't built raylib before, you just need a copy of the repo and type make in the folder.

# Snake Architecture
The importaint global variables are:
* `snake` a list of all the x‿y positions of the snake
* `fruit` where the fruit is on the board
* `facing` a list of x‿y, to be added to the head of the snake 
* `length` the length of the snake

Each time the snake moves the `⊑facing` is added to the `⊑snake` which is then attached to the front of the `snake` list.
The `length` is taken from this, this removes the end of the tail if the snake is too long.
If `⊑snake` is the same as `fruit` then increase the `length`.

The numbers in the input section are the keycodes for each key. 262, 263, 264, and 265 are the arrow keys and 334 is right shift.

# Raylib bindings
* `GetScreenSize` Takes no arguments and returns the screen size as x‿y
* `BeginDrawing` and `EndDrawing` delimiate the drawing section of the program, takes no arguments, returns @.
* `ClearBackground` takes a colour {u8,u8,u8,u8} and sets the background and returns @.
* `_drawText` takes 𝕨 text‿size, a 𝕗 which is a colour, and 𝕩 x‿y for the location of the text. Returns @.
* `_drawRect` takes a 𝕗 colour, and for 𝕩 x‿y‿w‿h and draws a rectangle at x‿y, w wide, h high.
* `_drawLine` takes a 𝕗 colour, and for 𝕩 x1‿y1‿x2‿y2 and draws a line between x1‿y1 and x2‿y2.
* `_runGame` takes a 𝕨 for the window name, 𝔽 for the main loop, and a x‿y for the size of the window.

The main loop should be a function for drawing, updating the game state, and taking input. For example `"Game window name" Draw∘Update∘Input rl._runGame 512‿512` would do.
